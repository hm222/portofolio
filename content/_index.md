+++
template = "index.html"

[extra]
about_me = "I'm Hanze Meng, a software developer with a passion for building scalable web applications, as well as a data management researcher who aims to help people learn relational queries."
my_name = "Hanze Meng"
current_year = 2024

[[extra.projects]]
title = "HNRQ - CInsGen"
description = "An interface helping novices characterizing single while differentiating pairs of relational queries."
url = "https://cinsgen.cs.duke.edu"

[[extra.projects]]
title = "HNRQ - I-Rex"
description = "A SQL Debugger focusing on relational query semantics."
url = "https://irex.cs.duke.edu"
+++