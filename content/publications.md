+++
template = "publications.html"

[extra]
my_name = "Hanze Meng"
+++

# My Publications

Welcome to my publications page! Here you can find a list of my works.

## 2023 Publications

- **Characterizing and Verifying Queries via CINSGEN**: An interface that enables users to obtain a comprehensive and compact view of all scenarios that satisfy a specified query, allowing for query characterization or distinction between two queries. [Read more](https://dl.acm.org/doi/10.1145/3555041.3589721).