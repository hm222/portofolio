+++
template = "projects.html"

[extra]
my_name = "Hanze Meng"
+++

# My Projects

Welcome to my projects page! Here you can find a list of my works.

## HNRQ Project

- **CInsGen**: An interface helping novices characterizing single while differentiating pairs of relational queries. [Try it](https://cinsgen.cs.duke.edu).

- **I-Rex**: A SQL Debugger focusing on relational query semantics. [Try it](https://irex.cs.duke.edu).

## DeepTracer Project

A website serving De Novo Macromolecular Complex Structure Prediction from Electron Density Maps, with frontend and backend implemented in Angular TypeScript and Gevent + Flask respectively.