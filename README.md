# My Portfolio Website

This repository contains the source code for my personal portfolio website, built using [Zola](https://www.getzola.org/), a fast static site generator.

![Alt text](image.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

1. **Clone the Repository**

   git clone https://gitlab.com/hm222/portofolio

   cd portofolio

2. **Run Zola**

   To start the server, run:

   zola serve

3. **Published Website**
   
   Go to this [link](https://hm222.gitlab.io/portofolio)

## Built With

- [Zola](https://www.getzola.org/) - The static site generator used